
// Users API url: https://jsonplaceholder.typicode.com/users
// Todos API url: https://jsonplaceholder.typicode.com/todos

// Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
// Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

// List 7 todos of 10 users and display them on the browser grouping them by User's Name. Example is shown below
// Rekha
// [ ] Make Biryani
// [ ] Board on Private jet

// Rajasekhar
// [ ] Buy Lamborghini
// [ ] Drive Bugatti Chiron
// ........
// ......

// To solve this follow these steps:
// First fetch users.
// After fetching users, use promise.all()  in parallel to fetch todos of each user.
// After promise.all is settled, take the values to form the final data for rendering.
// Make a template function using string literals and then use it to display the data on the browser.



fetch('https://jsonplaceholder.typicode.com/users').then((response)=> response.json())
.then((userData)=>{
    let userArrayPromise=userData.map((item)=>{
        return fetch(`https://jsonplaceholder.typicode.com/todos?userId=${item.id}`);
    })
    return Promise.all([userData,Promise.all(userArrayPromise)])
})
.then(([userData,todosList])=>{
    let userArrayPending=todosList.map((item)=>{
        return item.json();
    })
    return Promise.all([userData, Promise.all(userArrayPending)]);
})
.then(([user, todosDetail])=>{
        userSevenTodo=todosDetail.map((item)=>{
        return item.splice(0,7);
    })
    return ([user,userSevenTodo]);
})
.then(([userData,todosSeven])=>{
    todosSeven.forEach((item,index)=>{
        renderDom({
            userName:userData[index].name,
            todos:item,
        });
    })
})
.catch((error)=> console.log(error))

