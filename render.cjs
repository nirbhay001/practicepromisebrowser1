function renderDom(usersTodos) {
    let divId = document.getElementById("divId");
    let ul = document.createElement("ul");
    let header = document.createElement("h2");
    header.innerHTML = usersTodos.userName;
    usersTodos.todos.map((item) => {
        ul.innerHTML += `<li><input type="checkbox"><label>${item.title}</label></li>`
    })
    divId.appendChild(header);
    divId.appendChild(ul);
}


